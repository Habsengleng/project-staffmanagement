public class HourlyEmployee extends StaffMember{
    private int hoursWorked;
    private double rate;

    public HourlyEmployee(int id, String name, String address, int hoursWorked, double rate) {
        super(id, name, address);
        this.hoursWorked = hoursWorked;
        this.rate = rate;
    }

    public int getHoursWorked() {
        return hoursWorked;
    }

    public void setHoursWorked(int hoursWorked) {
        this.hoursWorked = hoursWorked;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    @Override
    public double pay() {
        if(hoursWorked<=0||rate<=0){
            return 0;
        }
        else{
            return hoursWorked*rate;
        }
    }

    @Override
    public String toString() {
        return super.toString()+"\nHours Worked: "+ hoursWorked + "h\nRate: $" + rate + "\nPayment: $" + pay();
    }
}
