public class SalariedEmployee extends StaffMember{
    private double salary;
    private double bonus;

    public SalariedEmployee(int id, String name, String address, double salary, double bonus) {
        super(id, name, address);
        this.salary = salary;
        this.bonus = bonus;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }

    @Override
    public String toString() {
        return super.toString()+"\nSalary: $"+salary + "\nBonus: $" + bonus + "\nPayment: $" + pay();
    }

    @Override
    public double pay() {
        if(salary<0||bonus<0){
            return 0;
        }
        else{
            return salary+bonus;
        }
    }
}
