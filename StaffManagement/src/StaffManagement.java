import java.util.*;

import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

public class StaffManagement {
    static ArrayList <Volunteer> arrVolunteer = new ArrayList<>();
    static ArrayList <SalariedEmployee> arrSalary = new ArrayList<>();
    static ArrayList <HourlyEmployee> arrHourly = new ArrayList<>();
    static ArrayList <StaffMember> array = new ArrayList<>();
    static Volunteer volunteer;
    static SalariedEmployee salariedEmployee;
    static HourlyEmployee hourlyEmployee;
    static Scanner cin = new Scanner(System.in);
    private static void initializeStaff(){
        volunteer = new Volunteer(1,"Pisey Som","PHNOM PENH");
        salariedEmployee = new SalariedEmployee(2,"Bopha Na","BATTAMBANG",500   ,70);
        hourlyEmployee = new HourlyEmployee(3,"Kompheak Sok","SIEM REAP",90,3);
        arrVolunteer.add(volunteer);
        arrSalary.add(salariedEmployee);
        arrHourly.add(hourlyEmployee);
        array.add(volunteer);
        array.add(salariedEmployee);
        array.add(hourlyEmployee);
    }
    private static void sortStaff(){
        Collections.sort(array, new Comparator<StaffMember>() {
            @Override
            public int compare(StaffMember o1, StaffMember o2) {
                int name1 = o1.getName().charAt(0);
                char name2 = o2.getName().charAt(0);
                int index = name1-name2;
                return index;
            }
        });
    }
    private static void showData(){
        Table t = new Table(1, BorderStyle.DESIGN_CURTAIN_HEAVY_WIDE, ShownBorders.ALL);
        for (StaffMember staff : array){
                t.addCell(staff.toString());
        }
        System.out.println(t.render());
    }
    private static void displayStaff(){
        sortStaff();
        showData();
    }
    private static void addVolunteerStaff(){
        boolean b=true;
        System.out.print("Enter Staff Member's ID: "); String vID = cin.nextLine();
        if(!checkNull(vID)&&checkValidNumber(vID)){
            for(int i=0;i<array.size();i++){
                if(Integer.parseInt(vID)==array.get(i).getId()){
                    b=false;
                    System.err.print("Duplicate ID");cin.nextLine();
                    break;
                }
            }
            if(b==true){
                System.out.print("Enter Staff Member's Name: "); String vName = cin.nextLine();
                if(!checkNull(vName)&&checkValidCharacter(vName)){
                    vName=capitalize(vName);
                    System.out.print("Enter Staff Member's Address: "); String vAddress = cin.nextLine().toUpperCase();
                    if(!checkNull(vAddress)){
                        volunteer = new Volunteer(Integer.parseInt(vID),vName,vAddress);
                        arrVolunteer.add(volunteer);
                        array.add(volunteer);
                    }else{
                        System.err.print("Please input valid address");
                        cin.nextLine();
                    }
                }else{
                    System.err.print("Please input valid name");
                    cin.nextLine();
                }
            }
        }else{
            System.err.print("Please input valid number");
            cin.nextLine();
        }
    }
    private static void addHourlyStaff(){
        boolean b=true;
        System.out.print("Enter Staff Member's ID: "); String hID = cin.nextLine();
        if(!checkNull(hID)&&checkValidNumber(hID)){
            for(int i=0;i<array.size();i++){
                if(Integer.parseInt(hID)==array.get(i).getId()){
                    b=false;
                    System.err.print("Duplicate ID");cin.nextLine();
                    break;
                }
            }
            if(b==true){
                System.out.print("Enter Staff Member's Name: "); String hName = cin.nextLine();
                if(!checkNull(hName)&&checkValidCharacter(hName)){
                    hName=capitalize(hName);
                    System.out.print("Enter Staff Member's Address: "); String hAddress = cin.nextLine().toUpperCase();
                    if(!checkNull(hAddress)){
                        System.out.print("Enter Hour worked: "); String hHourwork = cin.nextLine();
                        if(!checkNull(hHourwork)&&checkValidNumber(hHourwork)){
                            System.out.print("Enter rate: "); String hRate = cin.nextLine();
                            if(!checkNull(hRate)&&checkValidNumber(hRate)){
                                hourlyEmployee = new HourlyEmployee(Integer.parseInt(hID),hName,hAddress,Integer.parseInt(hHourwork),Double.parseDouble(hRate));
                                arrHourly.add(hourlyEmployee);
                                array.add(hourlyEmployee);
                            }else{
                                System.err.print("Please input valid number");
                                cin.nextLine();
                            }
                        }else{
                            System.err.print("Please input valid number");
                            cin.nextLine();
                        }
                    }else{
                        System.err.print("Please input valid Address");
                        cin.nextLine();
                    }
                }else{
                    System.err.print("Please input valid name");
                    cin.nextLine();
                }
            }
        }else{
            System.err.print("Please input valid number");
            cin.nextLine();
        }
    }
    private static void addSalariedStaff(){
        boolean b=true;
        System.out.print("Enter Staff Member's ID: "); String sID = cin.nextLine();
        if(!checkNull(sID)&&checkValidNumber(sID)){
            for(int i=0;i<array.size();i++){
                if(Integer.parseInt(sID)==array.get(i).getId()){
                    b=false;
                    System.err.print("Duplicate ID");cin.nextLine();
                    break;
                }
            }
            if(b==true){
                System.out.print("Enter Staff Member's Name: "); String sName = cin.nextLine();
                if(!checkNull(sName)&&checkValidCharacter(sName)){
                    sName=capitalize(sName);
                    System.out.print("Enter Staff Member's Address: "); String sAddress = cin.nextLine().toUpperCase();
                    if(!checkNull(sAddress)){
                        System.out.print("Enter Staff's Salary: "); String sSalary = cin.nextLine();
                        if(!checkNull(sSalary)&&checkValidNumber(sSalary)){
                            System.out.print("Enter Staff's Bonus: "); String sBonus = cin.nextLine();
                            if(!checkNull(sBonus)&&checkValidNumber(sBonus)){
                                salariedEmployee = new SalariedEmployee(Integer.parseInt(sID),sName,sAddress,Double.parseDouble(sSalary),Double.parseDouble(sBonus));
                                arrSalary.add(salariedEmployee);
                                array.add(salariedEmployee);
                            }else{
                                System.err.print("Please input valid number");
                                cin.nextLine();
                            }
                        }else{
                            System.err.print("Please input valid number");
                            cin.nextLine();
                        }
                    }else{
                        System.err.print("Please input valid Address");
                        cin.nextLine();
                    }
                }else{
                    System.err.print("Please input valid name");
                    cin.nextLine();
                }
            }
        }else{
            System.err.print("Please input valid number");
            cin.nextLine();
        }
    }
    private static void editStaff(){
        System.out.println("=======================Edit Info=======================");
        System.out.print("Enter Employee ID to Update: "); String eID = cin.nextLine();
        for(int i=0;i<arrVolunteer.size();i++){
            if(Integer.parseInt(eID)==arrVolunteer.get(i).getId()){
                System.out.println(arrVolunteer.get(i).toString());
                System.out.println("=======================New Information of STAFF MEMBER=======================");
                System.out.print("Enter new staff member's name: "); String vnName = cin.nextLine();
                if(!checkNull(vnName)&&checkValidCharacter(vnName)){
                    vnName = capitalize(vnName);
                    System.out.print("Enter new staff member's address: "); String vnAddress = cin.nextLine().toUpperCase();
                    if(!checkNull(vnAddress)){
                        arrVolunteer.get(i).setName(vnName);
                        arrVolunteer.get(i).setAddress(vnAddress);
                        break;
                    }else{
                        System.err.print("Please input valid Address");
                        cin.nextLine();
                    }
                }else{
                    System.err.print("Please input valid name");
                    cin.nextLine();
                }
            }
        }
        for(int i=0;i<arrHourly.size();i++){
            if(Integer.parseInt(eID)==arrHourly.get(i).getId()){
                System.out.println(arrHourly.get(i).toString());
                System.out.println("=======================New Information of STAFF MEMBER=======================");
                System.out.print("Enter Staff Member's Name: "); String hnName = cin.nextLine();
                if(!checkNull(hnName)&&checkValidCharacter(hnName)){
                    hnName=capitalize(hnName);
                    System.out.print("Enter Staff Member's Address: "); String hnAddress = cin.nextLine().toUpperCase();
                    if(!checkNull(hnAddress)){
                        System.out.print("Enter Hour worked: "); String hnHourwork = cin.nextLine();
                        if(!checkNull(hnHourwork)&&checkValidNumber(hnHourwork)){
                            System.out.print("Enter rate: "); String hnRate = cin.nextLine();
                            if(!checkNull(hnRate)&&checkValidNumber(hnRate)){
                                arrHourly.get(i).setName(hnName);
                                arrHourly.get(i).setAddress(hnAddress);
                                arrHourly.get(i).setHoursWorked(Integer.parseInt(hnHourwork));
                                arrHourly.get(i).setRate(Double.parseDouble(hnRate));
                                break;
                            }else{
                                System.err.print("Please input valid number");
                                cin.nextLine();
                            }
                        }else{
                            System.err.print("Please input valid number");
                            cin.nextLine();
                        }
                    }else{
                        System.err.print("Please input valid Address");
                        cin.nextLine();
                    }
                }else{
                    System.err.print("Please input valid name");
                    cin.nextLine();
                }
            }
        }
        for(int i=0;i<arrSalary.size();i++){
            if(Integer.parseInt(eID)==arrSalary.get(i).getId()){
                System.out.println(arrSalary.get(i).toString());
                System.out.println("=======================New Information of STAFF MEMBER=======================");
                System.out.print("Enter new staff member's name: "); String snName = cin.nextLine(); snName = capitalize(snName);
                if(!checkNull(snName)&&checkValidCharacter(snName)){
                    snName=capitalize(snName);
                    System.out.print("Enter Staff Member's Address: "); String snAddress = cin.nextLine().toUpperCase();
                    if(!checkNull(snAddress)){
                        System.out.print("Enter Staff's Salary: "); String snSalary = cin.nextLine();
                        if(!checkNull(snSalary)&&checkValidNumber(snSalary)){
                            System.out.print("Enter Staff's Bonus: "); String snBonus = cin.nextLine();
                            if(!checkNull(snBonus)&&checkValidNumber(snBonus)){
                                arrSalary.get(i).setName(snName);
                                arrSalary.get(i).setAddress(snAddress);
                                arrSalary.get(i).setSalary(Double.parseDouble(snSalary));
                                arrSalary.get(i).setBonus(Double.parseDouble(snBonus));
                                break;
                            }else{
                                System.err.print("Please input valid number");
                                cin.nextLine();
                            }
                        }else{
                            System.err.print("Please input valid number");
                            cin.nextLine();
                        }
                    }else{
                        System.err.print("Please input valid Address");
                        cin.nextLine();
                    }
                }else{
                    System.err.print("Please input valid name");
                    cin.nextLine();
                }
            }
        }
    }
    private static void removeStaff(){
        System.out.println("============================REMOVE Employee============================");
        System.out.print("Enter Employee ID to remove: "); String rID = cin.nextLine();
        for(int i=0;i<array.size();i++){
            if(Integer.parseInt(rID)==array.get(i).getId()){
                System.out.println(array.get(i).toString());
                array.remove(array.get(i));
                System.err.println("Remove sucessfully");
                break;
            }
        }
    }
    private static String capitalize(String str) {
        if(str == null || str.isEmpty()) {
            return str;
        }
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }
    private static boolean checkRange(int num,int min,int max){
        return (num<min||num>max);
    }
    private static boolean checkValidNumber(String num){
        return (num.matches("^[0-9]*$"));
    }
    private static boolean checkNull(String ch){
        return (ch.equals(""));
    }
    private static boolean checkValidCharacter(String ch){
        return (ch.matches("^[a-z A-Z]*$"));
    }
    public static void main(String[] args) {
        String st;
        String st1;
        initializeStaff();
        do{
            displayStaff();
            System.out.println("1. Add Employee");
            System.out.println("2. Edit");
            System.out.println("3. Remove");
            System.out.println("4. Exit");
            System.out.print("Choose option: "); st = cin.nextLine();
            if(!checkNull(st)&&checkValidNumber(st)&&!checkRange(Integer.parseInt(st),1,4)){
                switch (st){
                    case "1":
                        do{
                            System.out.println("°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°");
                            System.out.println("1. Volunteer");
                            System.out.println("2. Hourly Employee");
                            System.out.println("3. Salaried Employee");
                            System.out.println("4.Back");
                            System.out.print("Choose Option(1-4): "); st1 = cin.nextLine();
                                if(!checkNull(st1)&&checkValidNumber(st1)&&!checkRange(Integer.parseInt(st1),1,4)){
                                    switch (st1){
                                        case "1":
                                            addVolunteerStaff();
                                            break;
                                        case "2":
                                            addHourlyStaff();
                                            break;
                                        case "3":
                                            addSalariedStaff();
                                            break;
                                        case "4":break;
                                    }
                                }else{
                                    System.err.print("Please input a valid number");
                                    cin.nextLine();
                                }
                        }while(!st1.equals("4"));
                        break;
                    case "2":
                        editStaff();
                        break;
                    case "3":
                        removeStaff();
                        break;
                }
            }else{
                System.err.print("Please input a valid number");
                cin.nextLine();
            }
            if(st.equals("4")){
                System.out.print(Character.toChars(0x1f610));
                System.out.print(" Goodbye ");
                System.out.print(Character.toChars(0x1f610));
            }
            }while(!st.equals("4"));
        }
}
